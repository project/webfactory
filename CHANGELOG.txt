- Fix a cache problem when page_cache was enabled that make the results cached
  and not updated even if there was modifications.
- Fix warning entity_type in TableEntitiesForm L. 194.
- Fix "Undefined #element[pager] index" notice on the "Remote entities
  synchronize" page on slave.
- Fix compatibility with Drupal 8.1.0 (HTTP Method Options was bypass by new
  core OptionsRequestSubscriber introduced in 8.1.0).
- Fix filters on the "Remote entities synchronize" page that were not working
  correctly.
- Fix error when having more than one content type selected in a bundle channel
  that make crash the entities query (missing "IN" in condition).
- Fix a "Unique constraint violation" problem when importing a new node with an
  existing local nid on the satellite
- Add profile column in "My Websites" screen
