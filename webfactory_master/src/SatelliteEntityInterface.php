<?php

namespace Drupal\webfactory_master;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Satellite entity entities.
 */
interface SatelliteEntityInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
