<?php

namespace Drupal\webfactory;

/**
 * Webfactory generic exception.
 */
class WebfactoryException extends \Exception {
}
